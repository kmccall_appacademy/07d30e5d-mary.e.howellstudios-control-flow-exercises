# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
 str.gsub(/[^[:upper:]]+/, "")
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
   if str.length.even?
      return str[str.length/2-1] + str[str.length/2]
   else
      return str[str.length/2]
   end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.scan(/[aeoui]/).count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).inject(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  arr*separator
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(string)
  word_array = string.split(" ")
    word_array.each_with_index do |word, word_index|
    word.each_char.with_index do |char, char_index|
      char_index.even? ? word[char_index] = char.downcase : word[char_index] = char.upcase
    end
    word_array[word_index] = word
  end
  return word_array.join(" ")
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  storage = []
  x = str.split(' ')
  x.each do |word|
    if word.length >= 5
      storage << word.reverse
    else
      storage << word
    end
  end
  storage.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  storage = []
  (1..n).each do |i|
    if i % 3 == 0 && i % 5 == 0
      storage << 'fizzbuzz'
    elsif i % 3 == 0
      storage << 'fizz'
    elsif i % 5 == 0
      storage << 'buzz'
    else
      storage << i
    end
  end
  storage
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(a)
  (0...(a.length/2)).each {|i| a[i], a[a.length-i-1]=a[a.length-i-1], a[i]}
   return a
  end


# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num <= 1
    return false
  end

  idx = 2
  while idx < num
    if (num % idx) == 0
      return false
    end
    idx += 1
  end
  return true
end



# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select { |n|num % n == 0}
 end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(n)
     divisors = []
     divisor = 2

     while n > 1
       if n % divisor == 0
         n /= divisor
         divisors << divisor
       else
         divisor += 1
       end
     end

     divisors.uniq
 end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
evens = []
odds = []
  arr.each do |num|
    if num.even? == true
      evens << num
    elsif num.odd? == true
      odds << num
    end
  end
  if evens.count == 1
    return evens
  else
    return odds
  end
end
